
# Deploy configuration for NSA project (Epitech)

# Requirements (front/back)

* Debian 10 (Buster) hosts
* * open port 80

# Requirements (mysql)

* Debian 9 host
* ansible-galaxy install geerlingguy.mysql
* open port 3306
* password for BD : MYSQL_NSA_PASSWORD

# GCP VM configuration

 * create debian VM
 * assign static ip [link](https://console.cloud.google.com/networking/addresses/add?hl=fr&_ga=2.239913088.288085889.1581410378-1538846169.1579534745&_gac=1.225610344.1581410378.CjwKCAiAvonyBRB7EiwAadauqVB7cjcLSXD0rJmeKTA4Ag8C2e4O7S5BUQbmn1EgBns0T9XbkK5ZUxoCfqkQAvD_BwE&project=t-nsa-700-44&folder&organizationId)
 * allow ssh root login
 * add GitLab Runner VM public SSH Key to GCP (if not already done)
 * login one time from GitLab Runner VM to add to know_hosts